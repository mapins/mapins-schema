package org.mapins.schema.struct.config;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class StringValidatorConfig implements Cloneable{

    public enum StringSpecialFormat {
        Date,
        Time
    }
    @NotNull private Integer lengthLimit = 500;

    @Nullable private StringSpecialFormat format;

    public StringValidatorConfig() {}

    public StringValidatorConfig(@NotNull Integer lengthLimit) {
        this.lengthLimit = lengthLimit;
    }

    @JsonCreator
    public StringValidatorConfig(
            @JsonProperty("limit") @NotNull Integer lengthLimit,
            @JsonProperty("format") @Nullable StringSpecialFormat format
    ) {
        this.lengthLimit = lengthLimit;
        this.format = format;
    }

    public StringValidatorConfig(@Nullable StringSpecialFormat format) {
        this.format = format;
    }

    @JsonProperty("limit")
    public @NotNull Integer getLengthLimit(){
        return lengthLimit;
    }

    public void setLengthLimit(Integer limit) {
        if ( limit < 0 || limit > 500)
            throw new IllegalArgumentException("Limit can be only between 0 and up to 500 value.");

        this.lengthLimit = limit;
    }

    @JsonProperty("format")
    public @Nullable StringSpecialFormat getFormat(){
        return format;
    }

    public void setFormat(@Nullable StringSpecialFormat format){
        this.format = format;
    }

    @Override
    public StringValidatorConfig clone() {
        try {
            StringValidatorConfig clone = (StringValidatorConfig) super.clone();
            clone.lengthLimit = lengthLimit;
            clone.format = format;
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
