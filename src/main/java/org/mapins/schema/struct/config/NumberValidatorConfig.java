package org.mapins.schema.struct.config;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class NumberValidatorConfig implements Cloneable{
    @Nullable private Double fromValue;

    @Nullable private Double toValue;

    @NotNull private Integer precision = 2;

    @NotNull private Double step = 1.0;

    // default constructor for deserialization
    public NumberValidatorConfig() {}

    @JsonCreator
    public NumberValidatorConfig(
            @JsonProperty("from") @Nullable Double fromValue,
            @JsonProperty("to") @Nullable Double toValue,
            @JsonProperty("precision") @NotNull Integer precision,
            @JsonProperty("step") @Nullable Double step
    ){
        this.fromValue = fromValue;
        this.toValue = toValue;
        this.precision = precision;
        if (step != null)
            this.step = step;
    }

    public NumberValidatorConfig(
            @Nullable Double fromValue,
            @Nullable Double toValue,
            @NotNull Integer precision
    ){
        this.fromValue = fromValue;
        this.toValue = toValue;
        this.precision = precision;
    }

    public NumberValidatorConfig(@Nullable Double fromValue, @Nullable Double toValue){
        this.fromValue = fromValue;
        this.toValue = toValue;
    }

    @JsonProperty("from")
    public @Nullable Double getFromValue() {
        return fromValue;
    }

    public void setFromValue(@Nullable Double fromBorder) {
        this.fromValue = fromBorder;
    }

    @JsonProperty("to")
    public @Nullable Double getToValue() {
        return toValue;
    }

    public void setToValue(@Nullable Double toBorder) {
        this.toValue = toBorder;
    }

    @JsonProperty("precision")
    public @NotNull Integer getPrecision() {
        return precision;
    }

    public void setPrecision(@NotNull Integer precision) {
        this.precision = precision;
    }

    @JsonProperty("step")
    public @NotNull Double getStep() {
        return step;
    }

    public void setStep(@NotNull Double step) {
        this.step = step;
    }

    @Override
    public NumberValidatorConfig clone() {
        try {
            NumberValidatorConfig clone = (NumberValidatorConfig) super.clone();
            clone.fromValue = fromValue;
            clone.toValue = toValue;
            clone.step = step;
            clone.precision = precision;
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
