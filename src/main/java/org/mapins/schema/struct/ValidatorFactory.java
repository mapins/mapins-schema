package org.mapins.schema.struct;

import org.jetbrains.annotations.NotNull;

public interface ValidatorFactory {

    @NotNull DataValidator createValidator();
}
