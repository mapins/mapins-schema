package org.mapins.schema.struct.field;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mapins.schema.struct.DataValidator;
import org.mapins.schema.struct.JsonRepresented;
import org.mapins.schema.struct.exception.MapinsSchemaValidationException;
import org.mapins.schema.struct.ValidatorFactory;
import org.mapins.schema.struct.field.type.FieldType;

import java.util.Objects;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class SchemaField<T> implements ValidatorFactory, JsonRepresented {

    public static class FieldMetaInfo{

        @JsonProperty("str")
        public @Nullable String stringRepresentation;

        @JsonProperty("description")
        public @Nullable String description;

        @JsonProperty("index")
        public @Nullable Integer index;

        @JsonCreator
        public FieldMetaInfo(
            @JsonProperty("str") @Nullable String stringRepresentation,
            @JsonProperty("description") @Nullable String description,
            @JsonProperty("index") @Nullable Integer index
        ){
            this.stringRepresentation = stringRepresentation;
            this.description = description;
            this.index = index;
        }

        public FieldMetaInfo() {}

        boolean isEmpty(){
            return stringRepresentation == null && description == null && index == null;
        }
    }
    private @Nullable FieldMetaInfo metaInfo;
    private @NotNull Boolean isRequired;
    private @Nullable T defaultValue;

    private @NotNull FieldType fieldType;

    @JsonCreator
    public static @NotNull SchemaField<?> create(
            @JsonProperty("__metainf__") @Nullable FieldMetaInfo metaInfo,

            @JsonProperty("type") @NotNull FieldType fieldType,

            @JsonProperty("default") @NotNull Object defaultValue,

            @JsonProperty("required") @NotNull Boolean isRequired
    ) throws MapinsSchemaValidationException {

        fieldType.createValidator().validate(defaultValue);

        return new SchemaField<>(metaInfo, fieldType, defaultValue, isRequired);
    }

    public static @NotNull SchemaField<?> create(
            @Nullable FieldMetaInfo metaInfo,
            @NotNull FieldType fieldType,
            @NotNull Object defaultValue
    ) throws MapinsSchemaValidationException {

        fieldType.createValidator().validate(defaultValue);

        return new SchemaField<>(metaInfo, fieldType, defaultValue, true);
    }

    public static @NotNull SchemaField<?> create(
            @Nullable FieldMetaInfo metaInfo,
            @NotNull FieldType fieldType,
            @NotNull Boolean isRequired
    ) {
        return new SchemaField<>(metaInfo, fieldType, null, isRequired);
    }


    public static @NotNull SchemaField<?> create(
            @Nullable FieldMetaInfo metaInfo,
            @NotNull FieldType fieldType
    ) {
        return new SchemaField<>(metaInfo, fieldType, null, true);
    }

    public static @NotNull SchemaField<?> create(
            @NotNull FieldType fieldType,
            @NotNull Object defaultValue,
            @NotNull Boolean isRequired
    ) {
        return new SchemaField<>(null, fieldType, defaultValue, isRequired);
    }

    public static @NotNull SchemaField<?> create(
            @NotNull FieldType fieldType,
            @NotNull Object defaultValue
    ) {
        return new SchemaField<>(null, fieldType, defaultValue, true);
    }

    public static @NotNull SchemaField<?> create(
            @NotNull FieldType fieldType
    ) {
        return new SchemaField<>(null, fieldType, null, true);
    }

    public static @NotNull SchemaField<?> createFromString(@NotNull String json) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, SchemaField.class);
    }

    protected SchemaField(
            @Nullable FieldMetaInfo metaInfo,
            @NotNull FieldType type,
            @Nullable T defaultValue,
            @Nullable Boolean isRequired
    ){
        if (metaInfo != null && !metaInfo.isEmpty()){
            this.metaInfo = metaInfo;
        }
        this.fieldType = type;
        this.defaultValue = defaultValue;
        this.isRequired = Objects.requireNonNullElse(isRequired, false);
    }

    @JsonProperty("__metainf__")
    public @Nullable FieldMetaInfo getMetaInfo() {
        return metaInfo;
    }

    public void setMetaInfo(@Nullable FieldMetaInfo metaInfo) {
        this.metaInfo = metaInfo;
    }

    @JsonProperty("type")
    public @NotNull FieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType(@NotNull FieldType fieldType) {
        this.fieldType = fieldType;
    }

    @JsonProperty("required")
    public Boolean getRequired() {
        return isRequired;
    }

    public void setRequired(Boolean required) {
        isRequired = required;
    }

    @JsonProperty("default")
    public @Nullable T getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(@Nullable T defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * @return true if the field required implementation data(filling data from user) else false
     * */
    @JsonIgnore
    public boolean isFillRequired(){
        return defaultValue == null && isRequired;
    }

    @Override
    @JsonIgnore
    public @NotNull DataValidator createValidator() {
        class SchemaFiledValidator implements DataValidator{

            private final DataValidator fieldValidator = fieldType.createValidator();
            @Override
            public void validate(Object data) throws MapinsSchemaValidationException {
                if (data == null && isRequired && defaultValue == null)
                    throw new MapinsSchemaValidationException("Undefined required filed");
                else if (data == null)
                    return;

                fieldValidator.validate(data);
            }
        }

        return new SchemaFiledValidator();
    }
}
