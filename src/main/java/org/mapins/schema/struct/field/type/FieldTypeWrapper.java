package org.mapins.schema.struct.field.type;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.NotNull;

public abstract class FieldTypeWrapper extends FieldType{

    protected @NotNull FieldType subType;
    protected FieldTypeWrapper(@NotNull String typeName, @NotNull FieldType subType) {
        super(typeName);
        this.subType = subType;
    }

    @JsonProperty("subtype")
    public @NotNull FieldType getSubType(){
        return subType;
    }

    public void setSubType(@NotNull FieldType subType){
        this.subType = subType;
    }

}
