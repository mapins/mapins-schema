package org.mapins.schema.struct.field.type;


import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.mapins.schema.struct.JsonRepresented;
import org.mapins.schema.struct.ValidatorFactory;
import org.mapins.schema.struct.exception.MapinsSchemaValidationException;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "typename")
@JsonSubTypes({
        @JsonSubTypes.Type(value = NumberType.class, name = "Number"),
        @JsonSubTypes.Type(value = StringType.class, name = "String"),
        @JsonSubTypes.Type(value = EnumTypeWrapper.class, name = "enum")
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class FieldType implements ValidatorFactory, JsonRepresented {

    @NotNull private final String typeName;

    public static @NotNull FieldType createFromString(@NotNull String json) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, FieldType.class);
    }
    @JsonCreator
    protected FieldType( @JsonProperty("typename") @NotNull String typeName){
        this.typeName = typeName;
    }

    @JsonProperty("typename")
    public @NotNull String getTypename() {
        return typeName;
    }

    @JsonIgnore
    abstract protected @NotNull Class<?> getJavaClass();

    @JsonIgnore
    protected <T> void throwingCheckObjectJavaType(@NotNull Object object)
            throws MapinsSchemaValidationException {
        try {
            getJavaClass().cast(object);
        }
        catch (ClassCastException e){
            throw new MapinsSchemaValidationException("Value have incorrect type. Expected %s, received %s"
                    .formatted(getJavaClass().getSimpleName(), object.getClass().getSimpleName()));
        }
    }
}
