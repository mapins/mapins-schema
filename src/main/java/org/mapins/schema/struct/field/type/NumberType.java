package org.mapins.schema.struct.field.type;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mapins.schema.struct.DataValidator;
import org.mapins.schema.struct.exception.MapinsSchemaValidationException;
import org.mapins.schema.struct.config.NumberValidatorConfig;

public class NumberType extends FieldType {

    private @Nullable NumberValidatorConfig validatorConfig;

    @JsonCreator
    public static @NotNull NumberType create(
            @JsonProperty("validator") NumberValidatorConfig validatorConfig
    ){
        return new NumberType(validatorConfig);
    }

    public static @NotNull NumberType create(){
        return new NumberType();
    }

    protected NumberType() {
        super("Number");
    }
    protected NumberType(@NotNull NumberValidatorConfig validatorConfig ) {
        this();
        this.validatorConfig = validatorConfig;
    }

    @JsonProperty("validator")
    public @Nullable NumberValidatorConfig getValidatorConfig() {
        return validatorConfig;
    }

    public void setValidatorConfig(@NotNull NumberValidatorConfig validatorConfig) {
        this.validatorConfig = validatorConfig;
    }

    @Override
    @JsonIgnore
    protected @NotNull Class<?> getJavaClass() {
        return Number.class;
    }

    @Override
    @JsonIgnore
    public @NotNull DataValidator createValidator() {
        class NumberValidator implements DataValidator {

            final @NotNull NumberValidatorConfig config = validatorConfig != null ? validatorConfig.clone()
                    : new NumberValidatorConfig();
            @Override
            public void validate(Object data) throws MapinsSchemaValidationException {
                if (data == null)
                    return;

                throwingCheckObjectJavaType(data);

                double value = ((Number) data).doubleValue();

                if (config.getFromValue() != null && config.getFromValue() > value)
                    throw new MapinsSchemaValidationException("Number field violates lower value border.");

                if (config.getToValue() != null && config.getToValue() < value)
                    throw new MapinsSchemaValidationException("Number field violates upper value border.");

                if (config.getPrecision() <= 0){
                    String floatPartOfDouble = String.valueOf(value).split("\\.")[1];
                    if (floatPartOfDouble.length() > 1 || floatPartOfDouble.charAt(0) != '0')
                        throw new MapinsSchemaValidationException("Precision is too concrete");
                }
            }
        }
        return new NumberValidator();
    }
}
