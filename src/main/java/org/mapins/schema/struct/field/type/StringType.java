package org.mapins.schema.struct.field.type;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mapins.schema.struct.DataValidator;
import org.mapins.schema.struct.exception.MapinsSchemaValidationException;
import org.mapins.schema.struct.config.StringValidatorConfig;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class StringType extends FieldType {

    private @Nullable StringValidatorConfig validatorConfig;


    public static @NotNull StringType create() {
        return new StringType();
    }

    @JsonCreator
    public static @NotNull StringType create(
            @JsonProperty("validator") StringValidatorConfig validatorConfig
    ){
        return new StringType(validatorConfig);
    }
    protected StringType() {
        this(null);
    }

    protected StringType(@Nullable StringValidatorConfig validatorConfig) {
        super("String");
        this.validatorConfig = validatorConfig;
    }

    @JsonProperty("validator")
    public @Nullable StringValidatorConfig getValidatorConfig() {
        return validatorConfig;
    }

    public void setValidatorConfig(@Nullable StringValidatorConfig validatorConfig) {
        this.validatorConfig = validatorConfig;
    }

    @Override
    @JsonIgnore
    protected @NotNull Class<?> getJavaClass() {
        return String.class;
    }

    @Override
    @JsonIgnore
    public @NotNull DataValidator createValidator() {
        class StringValidator implements DataValidator{
            final @NotNull StringValidatorConfig config = validatorConfig != null ? validatorConfig.clone()
                    : new StringValidatorConfig();

            @Override
            public void validate(Object data) throws MapinsSchemaValidationException {
                if (data == null)
                    return;

                throwingCheckObjectJavaType(data);

                String value = (String) data;

                if (config.getFormat() != null){
                    var format = config.getFormat();
                    try {
                        switch (format){
                            case Date -> {
                                new SimpleDateFormat("dd.MM.yyyy").parse(value);
                            }
                            case Time -> {
                                new SimpleDateFormat("ss:mm:hh").parse(value);
                            }
                        }
                    }
                    catch (ParseException e) {
                            throw new MapinsSchemaValidationException(
                                    "String format is not correct");
                        }
                }
                else if (value.length() > config.getLengthLimit()){
                        throw new MapinsSchemaValidationException(
                                String.format(
                                        "String length validation failed." +
                                                " The length restriction violated. Expected max length %d, got %d",
                                        config.getLengthLimit(), value.length()
                                )
                        );
                }
            }
        }

        return new StringValidator();
    }
}