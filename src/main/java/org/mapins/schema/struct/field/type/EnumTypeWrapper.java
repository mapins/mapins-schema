package org.mapins.schema.struct.field.type;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.NotNull;
import org.mapins.schema.struct.DataValidator;
import org.mapins.schema.struct.exception.MapinsSchemaValidationException;
import org.mapins.schema.struct.exception.MapinsSchemaBuildException;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class EnumTypeWrapper extends FieldTypeWrapper {

    // The enum values
    @NotNull private Set<?> allowedValues;

    @JsonCreator
    public static @NotNull EnumTypeWrapper create(
            @JsonProperty("subtype") FieldType subType,
            @JsonProperty("enumSet") Collection<Object> values
    ) throws MapinsSchemaValidationException, MapinsSchemaBuildException {

        if (subType instanceof FieldTypeWrapper)
            throw new MapinsSchemaBuildException("The enum subtype can't be the wrapper field");

        if (values == null || values.isEmpty())
            throw new MapinsSchemaBuildException("The enum values can't be empty.");

        DataValidator validator = subType.createValidator();
        for (Object val: values){
            validator.validate(val);
        }

        return new EnumTypeWrapper(subType, values);
    }
    protected EnumTypeWrapper(FieldType subType, Collection<?> allowedValues) {
        super("enum", subType);
        this.subType = subType;
        this.allowedValues = new LinkedHashSet<>(allowedValues);
    };

    @JsonProperty("enumSet")
    public @NotNull Set<?> getAllowedValues() {
        return allowedValues;
    }

    public void setAllowedValues(@NotNull Collection<?> allowedValues) {
        this.allowedValues = new HashSet<>(allowedValues);
    }

    @Override
    @JsonIgnore
    protected @NotNull Class<?> getJavaClass() {
        return subType.getJavaClass();
    }

    @Override
    @JsonIgnore
    public @NotNull DataValidator createValidator() {
        final @NotNull Set<?> values = new LinkedHashSet<>(allowedValues);
        return ((obj) ->
        {
            if (!values.contains(obj))
                throw new MapinsSchemaValidationException("The selected value is not the enum value");
        });
    }
}
