package org.mapins.schema.struct.exception;

import org.jetbrains.annotations.NotNull;

public class MapinsSchemaBuildException extends Exception{
    public MapinsSchemaBuildException(){
        super();
    }

    public MapinsSchemaBuildException(@NotNull String message){
        super(message);
    }
}
