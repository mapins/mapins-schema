package org.mapins.schema.struct.exception;

import org.jetbrains.annotations.NotNull;

public class MapinsSchemaValidationException extends Exception{
    public MapinsSchemaValidationException(){
        super();
    }

    public MapinsSchemaValidationException(@NotNull String message){
        super(message);
    }
}
