package org.mapins.schema.struct;

import org.jetbrains.annotations.Nullable;
import org.mapins.schema.struct.exception.MapinsSchemaValidationException;

public interface DataValidator {
    void validate(@Nullable Object data) throws MapinsSchemaValidationException;
}
