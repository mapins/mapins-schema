package org.mapins.schema.struct;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mapins.schema.struct.exception.MapinsSchemaValidationException;
import org.mapins.schema.struct.field.SchemaField;

import java.util.*;
import java.util.stream.Collectors;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class Schema implements ValidatorFactory, JsonRepresented{
    public record SchemaMetaInfo(
            @Nullable String title,
            @Nullable String description,
            @Nullable String author,
            @Nullable UUID id
    ) {

        public SchemaMetaInfo(String title){
            this(title, null, null, null);
        }
        public SchemaMetaInfo(String title, String description){
            this(title, description, null, null);
        }
        public SchemaMetaInfo(String title, String description, String author){
            this(title, description, author, null);
        }

        public SchemaMetaInfo(UUID id){
            this(null, null, null, id);
        }

        public boolean isEmpty(){
            return title == null && description == null && author == null && id == null;
        }
    }

    private @Nullable SchemaMetaInfo metaInfo;

    private @NotNull String version = "1.0.0";

    private @NotNull Map<String, SchemaField<?>> fields = new LinkedHashMap<>();


    @JsonCreator
    public static @NotNull Schema create(
            @JsonProperty("__metainf__") @Nullable SchemaMetaInfo metaInfo,
            @JsonProperty("version") @Nullable String version,
            @JsonProperty("fields") @NotNull Map<String, SchemaField<?>> fields
    ){
        return new Schema(metaInfo, version, fields);
    }

    public static @NotNull Schema create(@Nullable SchemaMetaInfo metaInfo, @Nullable String version){
        return new Schema(metaInfo, version, null);
    }

    public static @NotNull Schema create(@NotNull SchemaMetaInfo metaInfo){
        return new Schema(metaInfo, null, null);
    }

    public static @NotNull Schema create(@NotNull Map<String, SchemaField<?>> fields){
        return new Schema(null, null, fields);
    }

    public static @NotNull Schema createFromString(@NotNull String json) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, Schema.class);
    }
    protected Schema(){}

    protected Schema(
            @Nullable SchemaMetaInfo metaInfo,
            @Nullable String version,
            @Nullable Map<String, SchemaField<?>> fields
    ){
        if (metaInfo != null && !metaInfo.isEmpty())
            this.metaInfo = metaInfo;
        if (version != null)
            this.version = version;
        if (fields != null)
            this.fields.putAll(fields);
    }

    @JsonProperty("__metainf__")
    public @Nullable SchemaMetaInfo getMetaInfo() {
        return metaInfo;
    }

    public void setMetaInfo(@Nullable SchemaMetaInfo metaInfo) {
        this.metaInfo = metaInfo;
    }

    @JsonProperty("version")
    public @NotNull String getVersion() {
        return version;
    }

    public void setVersion(@NotNull String version) {
        this.version = version;
    }

    @JsonProperty("fields")
    public @NotNull Map<String, SchemaField<?>> getFields() {
        return fields;
    }

    public void setFields(@NotNull Map<String, SchemaField<?>> fields) {
        this.fields = fields;
    }

    @JsonIgnore
    public void addField(String name, SchemaField<?> declaration){
        fields.put(name, declaration);
    }

    @JsonIgnore
    public SchemaField<?> removeField(String name){
        return fields.remove(name);
    }

    @JsonIgnore
    public Map<String, Object> processFullSchemaImplMap(Object obj) throws MapinsSchemaValidationException {
        createValidator().validate(obj);

        Map<String, Object> json = new LinkedHashMap<>(getObjectMap(obj));

        for (var field: fields.entrySet()){
            if (json.get(field.getKey()) == null && field.getValue().getRequired()){
                json.put(field.getKey(), field.getValue().getDefaultValue());
            }
            if (!json.containsKey(field.getKey())){
                json.put(field.getKey(), field.getValue().getDefaultValue());
            }
        }
        return json;
    }

    @JsonIgnore
    public String processFullSchemaImplJsonString(Object obj) throws MapinsSchemaValidationException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(processFullSchemaImplMap(obj));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public void clearSchemaFields(){
        fields.clear();
    }

    @Override
    @JsonIgnore
    public @NotNull DataValidator createValidator() {
        class SchemaValidator implements DataValidator{

            private final Map<String, DataValidator> fieldsValidators = fields.entrySet().stream()
                    .map(e -> Map.entry(e.getKey(), e.getValue().createValidator()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            @Override
            public void validate(Object data) throws MapinsSchemaValidationException {

                Map<String, Object> json;
                if (data != null)
                    json = getObjectMap(data);
                else
                    json = new HashMap<>();

                for (var dataField: json.entrySet()){
                    if (!fieldsValidators.containsKey(dataField.getKey()))
                        throw new MapinsSchemaValidationException(
                                "The data contain undefined '%s' schema filed"
                                        .formatted(dataField.getKey())
                        );

                    fieldsValidators.get(dataField.getKey()).validate(dataField.getValue());
                }
            }
        }
        return new SchemaValidator();
    }

    private Map<String, Object> getObjectMap(Object obj) throws MapinsSchemaValidationException {
        ObjectMapper mapper = new ObjectMapper();

        if (obj instanceof String){
            try {
                return mapper.readValue(((String) obj), new TypeReference<Map<String, Object>>() {});
            } catch (JsonProcessingException e) {
                throw new MapinsSchemaValidationException("String parse error: " + e.getMessage());
            }
        }
        else
            return mapper.convertValue(obj, new TypeReference<Map<String, Object>>() {});

    }

    /**
     * Check is schema has default implementation if the Mapins schema has weak definition which
     * not required the data from user
     * */
    @JsonIgnore
    public boolean hasDefaultImpl(){
        return fields.entrySet().stream()
                .noneMatch(f -> f.getValue().isFillRequired());
    }

    /**
     * @return minimal possible JSON as Map,
     * which represent possible implementation of MapinsSchema from their declaration
     * else return null (that means no possible implementations of Mapins Schema without user data filling)
     * */
    @JsonIgnore
    public @Nullable Map<@NotNull String, @Nullable Object> getDefaultImpl(){
        if (!hasDefaultImpl())
            return null;

        Map<String, Object> json = new HashMap<>();

        for (var e : fields.entrySet()){
            if (e.getValue().getDefaultValue() != null)
                json.put(e.getKey(), e.getValue().getDefaultValue());
        }

        return Collections.unmodifiableMap(json);
    }
}
