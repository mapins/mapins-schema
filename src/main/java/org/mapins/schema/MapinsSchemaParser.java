package org.mapins.schema;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mapins.schema.struct.Schema;
import org.mapins.schema.struct.exception.MapinsSchemaBuildException;

public class MapinsSchemaParser {

    private ObjectMapper mapper;
    public MapinsSchemaParser(){
        mapper = new ObjectMapper();
    }

    public Schema parse(String jsonString) throws MapinsSchemaBuildException {
        try {
            return mapper.readValue(jsonString, Schema.class);
        } catch (JsonProcessingException e) {
            throw new MapinsSchemaBuildException("Fail to process the json: " + e.getMessage());
        }
    }
}
