package org.mapins.schema;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;
import org.mapins.schema.struct.Schema;
import org.mapins.schema.struct.config.NumberValidatorConfig;
import org.mapins.schema.struct.config.StringValidatorConfig;
import org.mapins.schema.struct.exception.MapinsSchemaBuildException;
import org.mapins.schema.struct.exception.MapinsSchemaValidationException;
import org.mapins.schema.struct.field.SchemaField;
import org.mapins.schema.struct.field.type.EnumTypeWrapper;
import org.mapins.schema.struct.field.type.NumberType;
import org.mapins.schema.struct.field.type.StringType;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MapinsSchemaValidationsTest {

    @Nested
    class FieldTests {

        @Nested
        class NumberTypeTests{

            @Test
            void simple_Number_field_validation_test_without_config(){
                var numberFieldType = NumberType.create();

                assertDoesNotThrow(numberFieldType::createValidator);

                var validator = numberFieldType.createValidator();

                assertThrows(MapinsSchemaValidationException.class, () -> validator.validate("test"));
                assertDoesNotThrow(() -> validator.validate(1));
                assertDoesNotThrow(() -> validator.validate(1.));
                assertDoesNotThrow(() -> validator.validate(1f));
            }

            @Test
            void simple_Number_field_validation_test_with_config_for_double(){
                var numberFieldType = NumberType.create(
                        new NumberValidatorConfig(
                                0.,
                                100.
                        )
                );

                assertDoesNotThrow(numberFieldType::createValidator);

                var validator = numberFieldType.createValidator();

                assertThrows(MapinsSchemaValidationException.class, () -> validator.validate("test"));
                assertDoesNotThrow(() -> validator.validate(1));
                assertDoesNotThrow(() -> validator.validate(0));
                assertDoesNotThrow(() -> validator.validate(100));
                assertThrows(MapinsSchemaValidationException.class, () -> validator.validate(-1.));
                assertThrows(MapinsSchemaValidationException.class, () -> validator.validate(100.1));
            }

            @Test
            void simple_Number_field_validation_test_with_config_for_int(){
                var numberFieldType = NumberType.create(
                        new NumberValidatorConfig(
                                0.,
                                100.,
                                0
                        )
                );

                assertDoesNotThrow(numberFieldType::createValidator);

                var validator = numberFieldType.createValidator();

                assertThrows(MapinsSchemaValidationException.class, () -> validator.validate("1"));
                assertDoesNotThrow(() -> validator.validate(1));
                assertDoesNotThrow(() -> validator.validate(0));
                assertDoesNotThrow(() -> validator.validate(100));

                assertThrows(MapinsSchemaValidationException.class, () -> validator.validate(1.23));
                assertThrows(MapinsSchemaValidationException.class, () -> validator.validate(100.123));

                assertDoesNotThrow(() -> validator.validate(10));
                assertDoesNotThrow(() -> validator.validate(10.));
            }
        }

        @Nested
        class StringTypeTests{
            @Test
            void simple_String_type_validation_test_with_default_config(){
                var stringType = StringType.create();

                assertDoesNotThrow(stringType::createValidator);

                var validator = stringType.createValidator();

                assertThrows(MapinsSchemaValidationException.class, () -> validator.validate(1));
                assertDoesNotThrow(() -> validator.validate("test"));
            }

            @Test
            void simple_String_type_validation_test_with_set_config_to_Date_with_length_restriction(){
                var stringType = StringType.create(
                        new StringValidatorConfig(
                            1,
                                StringValidatorConfig.StringSpecialFormat.Date
                        )
                );

                assertDoesNotThrow(stringType::createValidator);

                var validator = stringType.createValidator();

                assertThrows(MapinsSchemaValidationException.class, () -> validator.validate(1));
                assertThrows(MapinsSchemaValidationException.class, () -> validator.validate("01,01,2023"));
                assertDoesNotThrow(() -> validator.validate("01.01.2023"));
            }

            @Test
            void simple_String_type_validation_test_with_set_config_to_Date_without_length_restriction(){
                var stringType = StringType.create(
                        new StringValidatorConfig(
                                StringValidatorConfig.StringSpecialFormat.Date
                        )
                );

                assertDoesNotThrow(stringType::createValidator);

                var validator = stringType.createValidator();

                assertThrows(MapinsSchemaValidationException.class, () -> validator.validate(1));
                assertThrows(MapinsSchemaValidationException.class, () -> validator.validate("01,01,2023"));
                assertDoesNotThrow(() -> validator.validate("01.01.2023"));
            }

            @Test
            void simple_String_type_validation_test_with_set_config_for_length_restriction(){
                var stringType = StringType.create(
                        new StringValidatorConfig(
                                1
                        )
                );

                assertDoesNotThrow(stringType::createValidator);

                var validator = stringType.createValidator();

                assertThrows(MapinsSchemaValidationException.class, () -> validator.validate(1));
                assertThrows(MapinsSchemaValidationException.class, () -> validator.validate("   "));
                assertThrows(MapinsSchemaValidationException.class, () -> validator.validate("aaa"));

                assertDoesNotThrow(() -> validator.validate(" "));
                assertDoesNotThrow(() -> validator.validate("a"));
            }
        }
    }

    @Nested
    class SchemaFieldTests{

        @Test
        void required_field_validation_with_number_type_without_default() throws MapinsSchemaValidationException {
            var field = SchemaField.create(NumberType.create());

            assertDoesNotThrow(field::createValidator);

            var validator = field.createValidator();

            assertThrows(MapinsSchemaValidationException.class, () -> validator.validate(null));
            assertThrows(MapinsSchemaValidationException.class, () -> validator.validate(""));

            assertDoesNotThrow(() -> validator.validate(1));
        }

        @Test
        void required_field_validation_with_number_type_with_default() throws MapinsSchemaValidationException {
            var field = SchemaField.create(NumberType.create(), 1);

            assertDoesNotThrow(field::createValidator);

            var validator = field.createValidator();

            assertThrows(MapinsSchemaValidationException.class, () -> validator.validate(""));

            assertDoesNotThrow(() -> validator.validate(null));
            assertDoesNotThrow(() -> validator.validate(1));
        }

        @Test
        void is_field_require_data_fill_check(){
            var field = SchemaField.create(NumberType.create(), 1, false);

            assertFalse(field.isFillRequired());
            assertNotNull(field.getDefaultValue());

            field = SchemaField.create(NumberType.create(), null, false);

            assertFalse(field.isFillRequired());
            assertNull(field.getDefaultValue());

            field = SchemaField.create(NumberType.create(), 1, true);

            assertFalse(field.isFillRequired());
            assertNotNull(field.getDefaultValue());

            field = SchemaField.create(NumberType.create(), null, true);

            assertTrue(field.isFillRequired());
            assertNull(field.getDefaultValue());
        }
    }

    @Nested
    class MapinsSchemaTests{

        @Test
        void empty_fields_schema_simple_test() throws MapinsSchemaValidationException, MapinsSchemaBuildException {
            var schema = Schema.create(
                    Map.of(
                            "number_field", SchemaField.create(NumberType.create()),
                            "string_field", SchemaField.create(StringType.create()),
                            "number_enum_field", SchemaField.create(EnumTypeWrapper.create(NumberType.create(),
                                    List.of(1,2,3)))
                    )
            );

            assertDoesNotThrow(schema::createValidator);

            var validator = schema.createValidator();

            assertDoesNotThrow(() -> validator.validate("""
                    {
                        "number_field": 0,
                        "string_field": "test",
                        "number_enum_field": 1
                    }
                    """)
            );

            record ObjectAnswerTestData(
                    @JsonProperty("number_field")
                    int numberField,

                    @JsonProperty("string_field")
                    String stringField,

                    @JsonProperty("number_enum_field")
                    int enumField
            ) {}

            assertDoesNotThrow(() -> validator.validate(
                    new ObjectAnswerTestData(0, "", 1))
            );

            assertDoesNotThrow(() -> validator.validate(
                    Map.of(
                        "number_field", 0,
                        "string_field", "",
                        "number_enum_field", 1
                    )
                )
            );
        }

        @Test
        void is_schema_require_data_fill_check() throws MapinsSchemaValidationException, MapinsSchemaBuildException {
            var schema = Schema.create(
                    Map.of(
                            "number_field", SchemaField.create(NumberType.create()),
                            "string_field", SchemaField.create(StringType.create())
                    )
            );
            assertFalse(schema.hasDefaultImpl());
            assertNull(schema.getDefaultImpl());

            schema = Schema.create(
                    Map.of(
                            "number_field", SchemaField.create(NumberType.create(), 1),
                            "string_field", SchemaField.create(StringType.create())
                    )
            );
            assertFalse(schema.hasDefaultImpl());
            assertNull(schema.getDefaultImpl());

            schema = Schema.create(
                    Map.of(
                            "number_field", SchemaField.create(NumberType.create(), null, true),
                            "string_field", SchemaField.create(StringType.create(), "test", true)
                    )
            );
            assertFalse(schema.hasDefaultImpl());
            assertNull(schema.getDefaultImpl());

            schema = Schema.create(
                    Map.of(
                            "number_field", SchemaField.create(NumberType.create(), 1.),
                            "string_field", SchemaField.create(StringType.create(), "test")
                    )
            );
            assertTrue(schema.hasDefaultImpl());
            assertNotNull(schema.getDefaultImpl());

            var schemaImpl = schema.getDefaultImpl();

            assertEquals(2, schemaImpl.size());

            assertTrue(schemaImpl.containsKey("number_field"));
            assertTrue(schemaImpl.get("number_field") instanceof Double);
            assertEquals(1.0, schemaImpl.get("number_field"));

            assertTrue(schemaImpl.containsKey("string_field"));
            assertTrue(schemaImpl.get("string_field") instanceof String);
            assertEquals("test", schemaImpl.get("string_field"));


            schema = Schema.create(
                    Map.of(
                            "number_field", SchemaField.create(NumberType.create(), 1.0),
                            "string_field", SchemaField.create(StringType.create(), null, false)
                    )
            );
            assertTrue(schema.hasDefaultImpl());
            assertNotNull(schema.getDefaultImpl());

            schemaImpl = schema.getDefaultImpl();

            assertEquals(1, schemaImpl.size());

            assertTrue(schemaImpl.containsKey("number_field"));
            assertTrue(schemaImpl.get("number_field") instanceof Double);
            assertEquals(1.0, schemaImpl.get("number_field"));

            assertFalse(schemaImpl.containsKey("string_field"));
            assertNull(schemaImpl.get("string_field"));
        }
    }
}
