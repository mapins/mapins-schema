package org.mapins.schema;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;
import org.mapins.schema.struct.Schema;
import org.mapins.schema.struct.config.NumberValidatorConfig;
import org.mapins.schema.struct.config.StringValidatorConfig;
import org.mapins.schema.struct.exception.MapinsSchemaBuildException;
import org.mapins.schema.struct.exception.MapinsSchemaValidationException;
import org.mapins.schema.struct.field.SchemaField;
import org.mapins.schema.struct.field.type.EnumTypeWrapper;
import org.mapins.schema.struct.field.type.NumberType;
import org.mapins.schema.struct.field.type.StringType;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MapinsSchemaSerializationTest {

    private final ObjectMapper mapper = new ObjectMapper();

    @Nested
    class FieldTests{

        @Nested
        class NumberTypeTest{

            @Test
            void serialize_empty_type() throws JsonProcessingException {
                var numberField = NumberType.create();

                final AtomicReference<String> json = new AtomicReference<>();
                assertDoesNotThrow(() -> {
                    json.set(mapper.writeValueAsString(numberField));
                });

                Map<String, Object> parse = mapper.readValue(json.get(), new TypeReference<Map<String, Object>>() {});

                assertEquals("Number", parse.get("typename"));
                assertNull(parse.get("validator"));
            }

            @Test
            void serialize_type_with_default_config() throws JsonProcessingException {
                var numberField = NumberType.create(new NumberValidatorConfig());

                final AtomicReference<String> json = new AtomicReference<>();
                assertDoesNotThrow(() -> {
                    json.set(mapper.writeValueAsString(numberField));
                });

                Map<String, Object> parse = mapper.readValue(json.get(), new TypeReference<Map<String, Object>>() {});

                assertEquals("Number", parse.get("typename"));
                assertNotNull(parse.get("validator"));

                final AtomicReference<Map<String, Object>> validatorConfig = new AtomicReference<>();
                assertDoesNotThrow(() -> validatorConfig.set((Map<String, Object>) parse.get("validator")));
                var config = validatorConfig.get();

                assertTrue(config.containsKey("step"));
                assertEquals(1., config.get("step"));

                assertTrue(config.containsKey("precision"));
                assertEquals(2, config.get("precision"));
            }

            @Test
            void serialize_type_with_custom_config() throws JsonProcessingException {
                var numberField = NumberType.create(
                        new NumberValidatorConfig(
                                0.,
                                100.,
                                0,
                                1.
                        )
                );

                final AtomicReference<String> json = new AtomicReference<>();
                assertDoesNotThrow(() -> {
                    json.set(mapper.writeValueAsString(numberField));
                });

                Map<String, Object> parse = mapper.readValue(json.get(), new TypeReference<Map<String, Object>>() {});

                assertTrue(() -> ((String)parse.get("typename")).equals("Number"));
                assertTrue(() -> parse.get("validator") != null);

                final AtomicReference<Map<String, Object>> validatorConfig = new AtomicReference<>();
                assertDoesNotThrow(() -> validatorConfig.set((Map<String, Object>) parse.get("validator")));
                var config = validatorConfig.get();

                assertTrue(config.containsKey("from"));
                assertEquals(0., config.get("from"));

                assertTrue(config.containsKey("to"));
                assertEquals(100., config.get("to"));

                assertTrue(config.containsKey("step"));
                assertEquals(1., config.get("step"));

                assertTrue(config.containsKey("precision"));
                assertEquals(0, config.get("precision"));
            }
        }

        @Nested
        class StringTypeTest{

            @Test
            void serialize_empty_type() throws JsonProcessingException {
                var numberField = StringType.create();

                final AtomicReference<String> json = new AtomicReference<>();
                assertDoesNotThrow(() -> {
                    json.set(mapper.writeValueAsString(numberField));
                });

                Map<String, Object> parse = mapper.readValue(json.get(), new TypeReference<Map<String, Object>>() {});

                assertEquals("String", parse.get("typename"));
                assertNull(parse.get("validator"));
            }

            @Test
            void serialize_type_with_default_config() throws JsonProcessingException {
                var numberField = StringType.create(new StringValidatorConfig());

                final AtomicReference<String> json = new AtomicReference<>();
                assertDoesNotThrow(() -> {
                    json.set(mapper.writeValueAsString(numberField));
                });

                Map<String, Object> parse = mapper.readValue(json.get(), new TypeReference<Map<String, Object>>() {});

                assertEquals("String", parse.get("typename"));
                assertNotNull(parse.get("validator"));

                final AtomicReference<Map<String, Object>> validatorConfig = new AtomicReference<>();
                assertDoesNotThrow(() -> validatorConfig.set((Map<String, Object>) parse.get("validator")));
                var config = validatorConfig.get();

                assertTrue(config.containsKey("limit"));
                assertEquals(500, config.get("limit"));
            }

            @Test
            void serialize_type_with_custom_config() throws JsonProcessingException {
                var numberField = StringType.create(
                        new StringValidatorConfig(
                                100,
                                StringValidatorConfig.StringSpecialFormat.Date
                        )
                );

                final AtomicReference<String> json = new AtomicReference<>();
                assertDoesNotThrow(() -> {
                    json.set(mapper.writeValueAsString(numberField));
                });

                Map<String, Object> parse = mapper.readValue(json.get(), new TypeReference<Map<String, Object>>() {});

                assertEquals("String", parse.get("typename"));
                assertNotNull(parse.get("validator"));

                final AtomicReference<Map<String, Object>> validatorConfig = new AtomicReference<>();
                assertDoesNotThrow(() -> validatorConfig.set((Map<String, Object>) parse.get("validator")));
                var config = validatorConfig.get();

                assertTrue(config.containsKey("format"));
                assertEquals("Date", config.get("format"));

                assertTrue(config.containsKey("limit"));
                assertEquals(100, config.get("limit"));
            }
        }
    }

    @Nested
    class TypeWrapperFields {
        @Nested
        class EnumWrapper {

            @Test
            void serialize_empty_wrapper_with_simple_default_subtype_and_zero_values()
                    throws MapinsSchemaValidationException, MapinsSchemaBuildException {
                final var enumField = new AtomicReference<EnumTypeWrapper>();

                assertThrows(MapinsSchemaBuildException.class,
                        () -> enumField.set(EnumTypeWrapper.create(NumberType.create(), List.of()))
                );
            }

            @Test
            void serialize_empty_wrapper_with_simple_default_subtype_and_one_incorrect_value()
                    throws MapinsSchemaValidationException, MapinsSchemaBuildException {
                final var enumField = new AtomicReference<EnumTypeWrapper>();

                assertThrows(MapinsSchemaValidationException.class,
                        () -> enumField.set(EnumTypeWrapper.create(NumberType.create(), List.of("test")))
                );
            }

            @Test
            void serialize_empty_wrapper_with_simple_default_subtype_and_one_correct_value()
                    throws MapinsSchemaValidationException, MapinsSchemaBuildException, JsonProcessingException {

                final var enumField = new AtomicReference<EnumTypeWrapper>();
                assertDoesNotThrow(() -> enumField.set(EnumTypeWrapper.create(NumberType.create(), List.of(1.23))));

                final AtomicReference<String> json = new AtomicReference<>();
                assertDoesNotThrow(() -> {
                    json.set(mapper.writeValueAsString(enumField.get()));
                });

                Map<String, Object> parse = mapper.readValue(json.get(), new TypeReference<Map<String, Object>>() {});

                assertEquals("enum", parse.get("typename"));
                assertTrue(parse.containsKey("subtype"));
                Map<String, Object> subtype = (Map<String, Object>) parse.get("subtype");
                assertEquals("Number", subtype.get("typename"));
                assertTrue(parse.containsKey("enumSet"));
                assertEquals(1, ((Collection<?>) parse.get("enumSet")).size());
            }
        }
    }

    @Nested
    class MapinsSchemaField{

        @Test
        void creating_schema_field_with_simple_type_minimal_initialization()
                throws MapinsSchemaValidationException, JsonProcessingException
        {
            final var field = new AtomicReference<SchemaField<?>>();
            assertDoesNotThrow(() -> field.set(SchemaField.create(NumberType.create())));

            final var json = new AtomicReference<String>();
            assertDoesNotThrow(() -> json.set(mapper.writeValueAsString(field.get())));

            Map<String, Object> parse = mapper.readValue(json.get(), new TypeReference<Map<String, Object>>() {});

            assertTrue(parse.containsKey("type"));
        }

        @Test
        void creating_schema_field_with_full_initialization_without_metainf()
                throws MapinsSchemaValidationException, JsonProcessingException {
            final var field = new AtomicReference<SchemaField<?>>();
            assertDoesNotThrow(() -> field.set(
                    SchemaField.create(
                            null,
                            NumberType.create(),
                            1,
                            true
                    ))
            );

            final var json = new AtomicReference<String>();
            assertDoesNotThrow(() -> json.set(mapper.writeValueAsString(field.get())));

            Map<String, Object> parse = mapper.readValue(json.get(), new TypeReference<Map<String, Object>>() {});

            assertEquals(1, parse.get("default"));
            assertEquals(true, parse.get("required"));

            assertTrue(() -> parse.containsKey("type"));
            var type = (Map<String, Object>)parse.get("type");
            assertEquals("Number", type.get("typename"));

            assertFalse(parse.containsKey("__metainf__"));
        }

        @Test
        void creating_schema_field_with_full_initialization_with_metainf()
                throws MapinsSchemaValidationException, JsonProcessingException {
            final var field = new AtomicReference<SchemaField<?>>();
            assertDoesNotThrow(() -> field.set(
                    SchemaField.create(
                            new SchemaField.FieldMetaInfo("test_field", "test", 1),
                            NumberType.create(),
                            1,
                            true
                    ))
            );

            final var json = new AtomicReference<String>();
            assertDoesNotThrow(() -> json.set(mapper.writeValueAsString(field.get())));

            Map<String, Object> parse = mapper.readValue(json.get(), new TypeReference<Map<String, Object>>() {});

            assertEquals(1, parse.get("default"));
            assertEquals(true, parse.get("required"));

            assertTrue(parse.containsKey("type"));
            var type = (Map<String, Object>)parse.get("type");
            assertEquals("Number", type.get("typename"));

            assertTrue(parse.containsKey("__metainf__"));
            var metainf = (Map<String, Object>)parse.get("__metainf__");
            assertEquals("test_field", metainf.get("str"));
            assertEquals("test", metainf.get("description"));
            assertEquals(1, metainf.get("index"));
        }
    }

    @Nested
    class MapinsSchemaTest{

        @Test
        void test_minimal_initialized_schema() throws JsonProcessingException {
            var schema = new AtomicReference<Schema>();
            assertDoesNotThrow(() -> {
                schema.set(Schema.create(null, null));
            });

            final var json = new AtomicReference<String>();
            assertDoesNotThrow(() -> json.set(mapper.writeValueAsString(schema.get())));

            Map<String, Object> parse = mapper.readValue(json.get(), new TypeReference<Map<String, Object>>() {});

            assertTrue(parse.containsKey("version"));
            assertTrue(parse.containsKey("fields"));
            assertFalse(parse.containsKey("__metainf__"));
        }

        @Test
        void test_minimal_initialized_schema_with_metainf() throws JsonProcessingException {
            var schema = new AtomicReference<Schema>();
            assertDoesNotThrow(() -> {
                schema.set(Schema.create(new Schema.SchemaMetaInfo(
                    "test_schema",
                        "test",
                        "Test T.T.",
                        new UUID(0, 0)
                    ),
                        "0"
                ));
            });

            final var json = new AtomicReference<String>();
            assertDoesNotThrow(() -> json.set(mapper.writeValueAsString(schema.get())));

            Map<String, Object> parse = mapper.readValue(json.get(), new TypeReference<Map<String, Object>>() {});

            assertEquals("0", parse.get("version"));
            assertTrue(parse.containsKey("fields"));

            assertTrue(parse.containsKey("__metainf__"));
            var metainf = (Map<String, Object>)parse.get("__metainf__");
            assertEquals("test_schema", metainf.get("title"));
            assertEquals("test", metainf.get("description"));
            assertEquals("Test T.T.", metainf.get("author"));
        }
    }
}
