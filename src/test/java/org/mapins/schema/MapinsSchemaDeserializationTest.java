package org.mapins.schema;

import org.junit.jupiter.api.*;
import org.mapins.schema.struct.Schema;
import org.mapins.schema.struct.exception.MapinsSchemaValidationException;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MapinsSchemaDeserializationTest {

    @Test
    void deserialize_simple_minimum_schema_from_string_source_by_MapinsSchemaParser(){
        var schema = new AtomicReference<Schema>();

        assertDoesNotThrow(() ->
            schema.set((new MapinsSchemaParser()).parse("""
                {
                  "version": "32",
                  "fields": {
                    "test": {
                      "type":{
                        "typename": "String"
                       }
                     }
                  }
                }
                """)
            )
        );

        assertDoesNotThrow(() ->
            schema.get().createValidator().validate("{\"test\":\"hello\"}")
        );
        assertThrows(MapinsSchemaValidationException.class,
                () -> schema.get().createValidator().validate("{\"test\": 123}")
        );
    }
}
